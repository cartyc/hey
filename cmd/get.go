// Copyright © 2018 NAME HERE <EMAIL ADDRESS>
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package cmd

import (
	"fmt"
	"net/http"
	"strconv"
	"sync"
	"time"

	"github.com/spf13/cobra"
)

// getCmd represents the get command
var getCmd = &cobra.Command{
	Use:   "get",
	Short: "Send traffic to specified url",

	Run: func(cmd *cobra.Command, args []string) {
		fmt.Println("Running...") // Give user some feedback
		var wg sync.WaitGroup     // Add waitgroup so it stays live

		req := cmd.Flag("n").Value.String()
		url := cmd.Flag("url").Value.String()

		count, _ := strconv.ParseInt(req, 10, 32)
		i := int(count)
		wg.Add(i)

		for x := 0; x < i; x++ {
			go MakeCall(url)
		}

		wg.Wait()
	},
}

// MakeCall Send traffic to target url
func MakeCall(url string) {

	// Keep looping
	for {
		req, err := http.Get(url)
		if err != nil {
			fmt.Println(err)
		}

		req.Body.Close()
		time.Sleep(100 * time.Millisecond) // Pause for 100 mili sec
	}

}

func init() {
	rootCmd.PersistentFlags().Int("n", 1, "Number of requests")
	rootCmd.PersistentFlags().String("url", "", "Request Url")
	rootCmd.AddCommand(getCmd)

	// Here you will define your flags and configuration settings.

	// Cobra supports Persistent Flags which will work for this command
	// and all subcommands, e.g.:
	// getCmd.PersistentFlags().String("foo", "", "A help for foo")

	// Cobra supports local flags which will only run when this command
	// is called directly, e.g.:
	// getCmd.Flags().BoolP("toggle", "t", false, "Help message for toggle")
}
